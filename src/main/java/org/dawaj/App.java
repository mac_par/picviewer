package org.dawaj;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.text.Text;
import javafx.scene.Scene;
import java.util.logging.Logger;
import javafx.stage.Screen;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

import org.dawaj.controller.ImageManager;
import org.dawaj.model.supplementary.DimensionRatio;
import org.dawaj.model.supplementary.FrameDetails;
import org.dawaj.view.ImageCollectorInterface;
import javafx.scene.layout.BorderPane;

public class App extends Application
{
	private final Logger m_logger=Logger.getLogger(getClass().getName());
	private BorderPane root=null;
	private Rectangle2D m_rectScreen;
	private static final double SCREEN_RATIO=0.5d;
	private ImageCollectorInterface imageManager;
	
	
    @Override
	public void init() throws Exception {
    	m_logger.info("Initialized");
    	Rectangle2D screen=Screen.getPrimary().getBounds();
    	double xCenter=screen.getWidth()-screen.getWidth()*SCREEN_RATIO;
    	double yCenter=screen.getHeight()-screen.getHeight()*SCREEN_RATIO;
    	m_rectScreen=new Rectangle2D(xCenter,yCenter,screen.getWidth()/2,screen.getHeight()*0.7d);
    	DimensionRatio ratioFrtScreen=new DimensionRatio(0.3,0.5);
    	ratioFrtScreen.setScreenSize(m_rectScreen.getWidth(),m_rectScreen.getHeight());
    	DimensionRatio ratioSndScreen=new DimensionRatio(0.6,0.5);
    	ratioSndScreen.setScreenSize(m_rectScreen.getWidth(),m_rectScreen.getHeight());
    	FrameDetails fdFrames=new FrameDetails(20,120);
    	FrameDetails fdFullImage=new FrameDetails(1,312.0d,512.0d);
    	imageManager=new ImageManager(ratioFrtScreen,fdFullImage,ratioSndScreen,fdFrames);
    	imageManager.setDefaultControls();
    	
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			root=new BorderPane();
		    Scene scene=new Scene(root,m_rectScreen.getWidth(),m_rectScreen.getHeight());
		    scene.getStylesheets().add(getClass().getResource("/css/applicationStyle.css").toExternalForm());
		    
		    //adding panes
		    root.setLeft(imageManager.getFullImageFlowPane());
		    root.setRight(imageManager.getSegmentedFlowPane());
		    root.setTop(getTitleText());
		    root.setBottom(getBottomHBox());
			primaryStage.setScene(scene);			
			primaryStage.setTitle("PicViewer");
			primaryStage.show();		
			imageManager.bindStage(primaryStage);
		}catch(Exception ex) {
			m_logger.throwing(getClass().getName(),"App::start", ex);
		}		
	}

	private Text getTitleText() {
		Text text=new Text("PicViewer v1.0");
		text.setId("lb_title_text");
		text.setFill(Color.WHITESMOKE);
		BorderPane.setAlignment(text, Pos.CENTER);
		return text;
	}
	private HBox getBottomHBox() {
		HBox box=new HBox(15.0d,this.imageManager.getClearSegmentsButton(),this.imageManager.getResetButton());
		BorderPane.setAlignment(box	, Pos.CENTER);
		return box;
	}
	
	@Override
	public void stop() throws Exception {
		imageManager.clearAll();
		super.stop();
	}

	public static void main( String[] args )
    {
        launch(args);
    }
}
