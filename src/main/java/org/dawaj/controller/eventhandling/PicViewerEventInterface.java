package org.dawaj.controller.eventhandling;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Node;
import java.util.Collection;
import java.util.Map;

public interface PicViewerEventInterface {
	void add(Node node);

	void addAll(Node... nodes);

	void addCollection(Collection<Node> collection);

	void clear();

	void setUp();

	void addEventHandler(Node node,EventType<? extends Event> eventType, EventHandler<? super Event> eventHandler)
			throws NullPointerException;

	void addAllEventHandlers(Node node,Map<EventType<? extends Event>, EventHandler<? super Event>> map) throws NullPointerException;
}
