package org.dawaj.controller.eventhandling;

import java.util.Collection;
import java.util.Map;
import java.util.Iterator;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Node;

public class PicViewerEventHandler extends AbstractEventHandler implements PicViewerEventInterface {

	public PicViewerEventHandler() {
		super();
	}

	public PicViewerEventHandler(Node node) {
		super();
		add(node);
	}

	@Override
	public void handle(Event event) {
		EventHandler<? super Event> handler = getNodeEventHandler((Node) event.getSource(), event.getEventType());
		if (handler != null)
			handler.handle(event);
	}

	@Override
	public void add(Node node) {
		if (node != null) {
			addNodeElement(node);
		}
	}

	@Override
	public void addAll(Node... nodes) {
		for (int i = 0; i < nodes.length; i++) {
			add(nodes[i]);
		}
	}

	@Override
	public void addCollection(Collection<Node> collection) {
		Iterator<Node> iter = collection.iterator();
		while (iter.hasNext())
			add(iter.next());
	}

	@Override
	public void clear() {
		removeEvents();
		removeNodes();
	}

	@Override
	public void setUp() {
		setEventHandlers();
	}

	@Override
	public void addEventHandler(Node node, EventType<? extends Event> eventType,
			EventHandler<? super Event> eventHandler) throws NullPointerException {
		if (eventType != null && eventHandler != null)
			addEventByNode(node, eventType, eventHandler);
	}

	@Override
	public void addAllEventHandlers(Node node, Map<EventType<? extends Event>, EventHandler<? super Event>> map)
			throws NullPointerException {
		if (map.isEmpty())
			return;
		else {
			map.entrySet().parallelStream().forEach(entry -> {
				addEventByNode(node, entry.getKey(), entry.getValue());
			});
		}
	}

}
