package org.dawaj.controller.eventhandling;

import javafx.event.Event;
import javafx.event.EventType;
import javafx.event.EventHandler;
import java.util.Collections;
import java.util.Map;
import java.util.HashMap;
import java.util.TreeMap;
import javafx.scene.Node;

abstract public class AbstractEventHandler implements EventHandler<Event> {
	private final Map<Node, Map<EventType<? extends Event>, EventHandler<? super Event>>> mapEvents;

	public AbstractEventHandler() {
		mapEvents = Collections.synchronizedMap(new HashMap<>());
	}

	protected synchronized void addEventByNode(Node node, EventType<? extends Event> eventType,
			EventHandler<? super Event> eventHandler) throws NullPointerException {
		if (mapEvents != null) {
			mapEvents.computeIfAbsent(node, key -> Collections.synchronizedMap(new HashMap<>())).computeIfAbsent(
					eventType, key->eventHandler);
		}
	}

	protected synchronized void removeEvents() throws NullPointerException {
		if (!mapEvents.isEmpty()) {

			for (Node node : mapEvents.keySet()) {
				mapEvents.get(node).entrySet().parallelStream().forEach(entry->{
					if(entry != null)
					node.removeEventHandler(entry.getKey(), entry.getValue());
				});
				mapEvents.get(node).clear();
			}
		}
	}

	protected synchronized void setEventHandlers() {
		if (mapEvents != null && !mapEvents.isEmpty()) {
			mapEvents.entrySet().parallelStream().forEach(entry->{
				Node node=entry.getKey();
				entry.getValue().entrySet().parallelStream().forEach(evSet->{
					node.addEventHandler(evSet.getKey(), evSet.getValue());
				});
			});
		}
	}

	protected synchronized void removeNodes() {
		if (mapEvents != null && !mapEvents.isEmpty()) {
			mapEvents.clear();
		}
	}

	protected void addNodeElement(Node node) {
		if (node != null)
			mapEvents.computeIfAbsent(node, key->Collections.synchronizedMap(new HashMap<>()));
	}

	protected synchronized EventHandler<? super Event> getNodeEventHandler(Node node,
			EventType<? extends Event> eventType) {
		EventHandler<? super Event> eventHandler = null;
		
		if (node != null && !mapEvents.isEmpty())
			eventHandler = mapEvents.get(node).get(eventType);
		return eventHandler;
	}

	abstract public void handle(Event event);

}
