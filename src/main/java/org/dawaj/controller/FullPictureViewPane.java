package org.dawaj.controller;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.stage.FileChooser;
import org.dawaj.model.image_utils.SizeProperty;
import org.dawaj.model.image_utils.FullImageUnit;
import java.util.Arrays;
import javafx.scene.input.MouseEvent;
import javafx.scene.Cursor;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.image.PixelWriter;
import javafx.geometry.Point2D;
import org.dawaj.view.FullPictureControlInterface;
import org.dawaj.view.SegmentedPaneControlInterface;

import java.io.File;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineJoin;
import javafx.scene.shape.StrokeType;
import javafx.stage.Stage;
import java.io.FilenameFilter;
import java.io.IOException;

import org.dawaj.controller.eventhandling.PicViewerEventHandler;
import org.dawaj.controller.eventhandling.PicViewerEventInterface;
import org.dawaj.model.image_utils.ImageRect;
import org.dawaj.model.supplementary.DimensionRatio;
import org.dawaj.model.supplementary.FrameDetails;
import javafx.scene.layout.FlowPane;
import java.util.TreeSet;
import java.util.Collections;
import java.util.NavigableSet;
import java.util.SortedSet;
import java.util.logging.Logger;
import java.util.Properties;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class FullPictureViewPane implements FullPictureControlInterface {
	private final FlowPane flowPane;
	private final PicViewerEventInterface eventHandler;
	private final NavigableSet<File> setImageFiles;
	private File fileCurrent = null, fileMainDir = null;
	private final ImageRect rectDisplay;
	private DimensionRatio drPaneRatio = null;
	private Stage stageRoot = null;
	private FileChooser fileChooser = null;
	private Logger logPane = null;
	private final SegmentedPaneControlInterface segmentedPane;
	private final FrameDetails fdFrame;

	public FullPictureViewPane(SegmentedPaneControlInterface segmentedPane) {
		this.segmentedPane = segmentedPane;
		setImageFiles = Collections.synchronizedNavigableSet(new TreeSet<>());
		this.fdFrame = new FrameDetails(1, IMAGE_WIDTH_DEF, IMAGE_HEIGHT_DEF);
		FullImageUnit fullImageUnit = new FullImageUnit(new SizeProperty(IMAGE_WIDTH_DEF, IMAGE_HEIGHT_DEF));
		rectDisplay = new ImageRect(fullImageUnit);
		eventHandler = new PicViewerEventHandler();
		flowPane = new FlowPane(Orientation.VERTICAL);
		init();
	}

	public FullPictureViewPane(SegmentedPaneControlInterface segmentedPane, DimensionRatio dSndPaneRatio,
			FrameDetails fdFullImagePane) {
		this.segmentedPane = segmentedPane;
		setImageFiles = Collections.synchronizedNavigableSet(new TreeSet<>());
		this.fdFrame = fdFullImagePane;
		rectDisplay = new ImageRect(
				new FullImageUnit(new SizeProperty(fdFullImagePane.getWidth(), fdFullImagePane.getHeight())));

		eventHandler = new PicViewerEventHandler();
		flowPane = new FlowPane(Orientation.VERTICAL);
		this.drPaneRatio = dSndPaneRatio;
		init();
	}

	private void init() {
		setUpFlowPane();
		setUpRectDisplay();
		setUpEvents();
		loadImageFiles();
	}

	// setting canvas events handling
	private void setUpEvents() {

		eventHandler.addEventHandler(rectDisplay, MouseEvent.MOUSE_CLICKED, ev -> {
			if (ev instanceof MouseEvent) {
				MouseEvent event = (MouseEvent) ev;
				if (event.getClickCount() >= 2) {
					Point2D point = new Point2D(checkPointCorrectness(event.getX()),
							checkPointCorrectness(event.getY()));
					this.segmentedPane.addImage(this.fileCurrent, point);
				}
			}
		});
		eventHandler.addEventHandler(rectDisplay, MouseEvent.MOUSE_ENTERED, ev -> {
			if (this.stageRoot != null) {
				this.stageRoot.getScene().setCursor(Cursor.CROSSHAIR);
			}
		});
		eventHandler.addEventHandler(rectDisplay, MouseEvent.MOUSE_EXITED, ev -> {
			if (this.stageRoot != null) {
				this.stageRoot.getScene().setCursor(Cursor.DEFAULT);
			}
		});
		eventHandler.setUp();
	}

	private double checkPointCorrectness(double input) {
		return (Double.compare(input, 0.0) == -1) ? 0.0 : input;
	}

	private void setUpRectDisplay() {
		rectDisplay.setMockImageHandler(this::getBlankImage);
		rectDisplay.reset();
		rectDisplay.setStroke(Color.BLACK);
		rectDisplay.setStrokeWidth(8);
		rectDisplay.setStrokeType(StrokeType.OUTSIDE);
		rectDisplay.setStrokeLineJoin(StrokeLineJoin.MITER);
		rectDisplay.turnOffTooltip();
		flowPane.getChildren().add(rectDisplay);
	}

	protected void setUpFlowPane() {
		if (this.drPaneRatio != null) {
			flowPane.setPrefWidth(drPaneRatio.getScreenDimensions().getWidth());
			flowPane.setPrefHeight(drPaneRatio.getScreenDimensions().getHeight());
			double dHGap = (flowPane.getWidth() - rectDisplay.getWidth()) * 0.5;
			double dVGap = (flowPane.getHeight() - rectDisplay.getHeight()) * 0.5;
			flowPane.setHgap(dHGap);
			flowPane.setVgap(dVGap);
			FlowPane.setMargin(rectDisplay, new Insets(15.0));
		} else {
			flowPane.setHgap(HORIZONTAL_PADDING);
			flowPane.setVgap(VERTICAL_PADDING);
		}

	}

	protected void setUpFileChooser() {
		if (fileChooser == null) {
			fileChooser = new FileChooser();
			fileChooser.setTitle("Choose an image");
			fileChooser.setInitialDirectory(
					(this.fileMainDir == null) ? new File(System.getProperty("user.home")) : fileMainDir);
			fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("All extensions", "*.*"),
					new FileChooser.ExtensionFilter("JPEG files", "*.jpeg", "*.jpg"),
					new FileChooser.ExtensionFilter("PNG files", "*.png"),
					new FileChooser.ExtensionFilter("GIF files", "*.gif"));
		}
	}

	protected void addImageFile(File file) {

		if (setImageFiles.add(file)) {
			moveToFile(file);
			if (this.fileMainDir == null || !this.fileMainDir.equals(file.getParentFile())) {
				this.fileMainDir = file.getParentFile();
			}
		}
	}

	protected void loadFile(File file) {
		if (this.setImageFiles != null) {
			this.setImageFiles.add(file);
		}
	}

	@Override
	public void begin() {
		moveToFile((setImageFiles.isEmpty()) ? null : setImageFiles.first());
	}

	@Override
	public void end() {
		moveToFile((setImageFiles.isEmpty()) ? null : setImageFiles.last());
	}

	@Override
	public void next() {
		choosePicture(true);
	}

	@Override
	public void previous() {
		choosePicture(false);
	}

	@Override
	public void pickPiture() {
		if (stageRoot != null) {
			selectPicture();
		} else {
			logPane.warning("stage property wasnt binded");
		}
	}

	protected void selectPicture() {
		if (fileChooser != null) {
			File imageFile = fileChooser.showOpenDialog(stageRoot);
			if (imageFile != null && imageFile.exists())
				addImageFile(imageFile);
		}
	}

	@Override
	public void clearAll() {
		eventHandler.clear();
		rectDisplay.clear();
		saveImageFiles();
	}

	@Override
	public void clean() {
		rectDisplay.reset();
		this.fileCurrent = null;
		this.setImageFiles.clear();
	}

	@Override
	public final Pane getPane() {
		return flowPane;
	}

	private Image getBlankImage() {
		WritableImage mockImage = new WritableImage((int) rectDisplay.getWidth(), (int) rectDisplay.getHeight());
		PixelWriter pixWriter = mockImage.getPixelWriter();
		for (int x = 0; x < mockImage.getWidth(); x++) {
			for (int y = 0; y < mockImage.getHeight(); y++) {
				pixWriter.setColor(x, y, Color.TRANSPARENT);
			}
		}
		return mockImage;
	}

	@Override
	public void bindStage(Stage stage) {
		if (stage != null) {
			this.stageRoot = stage;
			setUpFileChooser();
		}
	}

	private void choosePicture(boolean bNext) {
		if (setImageFiles != null && !setImageFiles.isEmpty()) {
			SortedSet<File> setTmp = (bNext) ? setImageFiles.tailSet(fileCurrent, false)
					: setImageFiles.headSet(fileCurrent, false);
			if (setTmp.isEmpty()) {
				moveToFile((bNext) ? setImageFiles.first() : setImageFiles.last());
			} else {
				moveToFile((bNext) ? setTmp.first() : setTmp.last());
			}
		}
	}

	private void moveToFile(File file) {
		if (file == null || !file.exists())
			return;

		rectDisplay.setFile(file);
		fileCurrent = file;
		
	}

	@Override
	public void setLogger(Logger logger) {
		if (logger != null)
			this.logPane = logger;
	}

	@Override
	public void getFilesAt(String path) {
		File file = null;
		try {
			file = new File(path);
			// if path does not exist or is not a dir, then use home dir
			if (!file.exists() || !file.isDirectory()) {
				file = new File(System.getProperty("user.home"));
			}

			File[] fGroup = file.listFiles(new FilenameFilter() {

				@Override
				public boolean accept(File dir, String name) {
					return isCorrectExt(name);
				}
			});
			// iterate through list
			for (File fImage : fGroup) {
				this.loadFile(fImage);
			}
		} catch (Exception ex) {
			logPane.throwing(getClass().getName(), "getFilesAt", ex);
		} finally {
			begin();
			if (file != null) {
				this.fileMainDir = file;
				logPane.info("main directory set at: " + this.fileMainDir);
			}
		}
	}

	@Override
	public FrameDetails getFrameDetails() {
		return this.fdFrame;
	}

	private void loadImageFiles() {
		Properties propFiles = new Properties();
		String strRelPath = String.format("%s%c%s", MAIN_DIR, File.separatorChar, MAIN_FILE);
		File fSettings = new File(System.getProperty("user.home"), strRelPath);

		// load properties from file
		try (FileInputStream fis = new FileInputStream(fSettings)) {
			propFiles.load(fis);

			// initialize files set
			Integer nImages = Integer.parseInt(propFiles.getProperty(PROPS_HEAD_KEY, "0"));
			if (nImages.compareTo(0) == 0) {
				return;
			} else {
				for (int i = 0; i < nImages; i++) {
					String strImg = String.format(PROPS_IMG_KEY + PROPS_IMG_KEY_FORMAT, i);
					File fImage = new File(propFiles.getProperty(strImg));
					if (fImage.exists() && this.isCorrectExt(fImage.getName())) {
						this.setImageFiles.add(fImage);
					}
				}
				// from previously displayed image or from the beginning
				String strLastImage = propFiles.getProperty(PROPS_IMG_KEY + PROPS_IMG_LAST);
				if (strLastImage == null || !isCorrectExt(strLastImage) || !new File(strLastImage).exists())
					this.begin();
				else {
					this.moveToFile(new File(strLastImage));
				}
				// set main directory if img.main_dir is among properties, otherwise leave home
				// directory
				String strMainDir = propFiles.getProperty(PROPS_MAIN_DIR);
				if ((strMainDir != null && new File(strMainDir).exists())
						&& !strMainDir.equals(System.getProperty("user.home"))) {
					this.fileMainDir=new File(strMainDir);
				}

			}
		} catch (FileNotFoundException ex) {
			Logger.getGlobal().throwing(this.getClass().getName(), "loadImageFile", ex);
		} catch (IOException ex) {
			Logger.getGlobal().throwing(this.getClass().getName(), "loadImageFile", ex);
		}
	}

	private void saveImageFiles() {
		Properties propSettings = new Properties();
		propSettings.setProperty(PROPS_HEAD_KEY, Integer.toString(this.setImageFiles.size()));
		int nImg = 0;
		for (File file : this.setImageFiles) {
			String key = String.format(PROPS_IMG_KEY + PROPS_IMG_KEY_FORMAT, nImg);
			propSettings.setProperty(key, file.getPath());
			nImg++;
		}
		if (this.fileCurrent != null) {
			propSettings.setProperty(PROPS_IMG_KEY + PROPS_IMG_LAST, this.fileCurrent.getPath());
		}
		// save to output
		String strPath = String.format("%s%c%s", MAIN_DIR, File.separatorChar, MAIN_FILE);
		if(!this.fileMainDir.getPath().equals(System.getProperty("user.home"))) {
			propSettings.setProperty(PROPS_MAIN_DIR, this.fileMainDir.getPath());
		}
		
		File fSettings = null;
		try {
			fSettings = new File(System.getProperty("user.home"), strPath);
			if (!fSettings.exists()) {
				if (!fSettings.getParentFile().exists())
					fSettings.getParentFile().mkdirs();
				fSettings.createNewFile();
			}
			try (FileOutputStream fos = new FileOutputStream(fSettings)) {
				propSettings.store(fos, "PicViewer settings");
			}
		} catch (FileNotFoundException ex) {
			Logger.getGlobal().throwing(this.getClass().getName(), "saveImageFiles", ex);
		} catch (Exception ex) {
			Logger.getGlobal().throwing(this.getClass().getName(), "saveImageFiles", ex);
		} finally {
			setImageFiles.clear();
		}
	}

	private final boolean isCorrectExt(String filename) {
		for (String ext : Arrays.asList(EXTENSIONS.split(":"))) {
			if (filename.endsWith(ext)) {
				return true;
			}
		}
		return false;
	}
}
