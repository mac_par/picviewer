package org.dawaj.controller.navigation;

import org.dawaj.controller.eventhandling.PicViewerEventHandler;
import org.dawaj.controller.eventhandling.PicViewerEventInterface;
import org.dawaj.view.FullPictureControlInterface;
import org.dawaj.view.SegmentedPaneControlInterface;
import org.dawaj.model.supplementary.FrameDetails;
import org.dawaj.model.supplementary.RGBEnum;
import java.util.EnumSet;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Pos;
import javafx.collections.FXCollections;
import javafx.scene.input.MouseEvent;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Shape;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;
import javafx.scene.shape.StrokeType;
import java.util.Map;
import java.util.HashMap;

public class NavigationManager {
	private FullPictureControlInterface fullImagePane;
	private SegmentedPaneControlInterface segmentsPane;
	private PicViewerEventInterface eventHandler;
	private ComboBox<RGBEnum> cbColorMenu;
	private Button btnClear = null, btnReset = null;
	// temporaty buttons
	private Path leftArrowPath = null, rightArrowPath = null;
	private Shape innerCircle = null;
	private DropShadow shadowEffect = null;

	public NavigationManager(FullPictureControlInterface fpcHandler, SegmentedPaneControlInterface spcHandler) {
		this.fullImagePane = fpcHandler;
		this.segmentsPane = spcHandler;
		ObservableList<RGBEnum> listRGBEnum = FXCollections.observableArrayList(EnumSet.allOf(RGBEnum.class));
		this.cbColorMenu = new ComboBox<>(listRGBEnum);
		this.eventHandler = new PicViewerEventHandler();
	}

	private void initControls() {
		this.cbColorMenu.setValue(RGBEnum.RED);
		this.cbColorMenu.setEditable(false);
		this.cbColorMenu.setPromptText("Choose an option");
		this.cbColorMenu.valueProperty().addListener((obs, oldValue, newValue) -> {
			segmentsPane.setRGBDescriptor(newValue);
		});
		
		// initialize btns

		this.btnClear = new Button("Clear");
		this.eventHandler.addEventHandler(this.btnClear, MouseEvent.MOUSE_CLICKED, event -> {
			if (this.segmentsPane != null && this.fullImagePane != null) {
				this.fullImagePane.clean();
				this.segmentsPane.clean();
			}
		});

		this.btnReset = new Button("Reset");
		this.eventHandler.addEventHandler(this.btnReset, MouseEvent.MOUSE_CLICKED, event -> {
			if (this.segmentsPane != null) {
				this.segmentsPane.clean();
			}
		});
		// arrows responsible for next(right) and previous(left), and pick sign
		setUpShapes();

		this.eventHandler.setUp();
	}

	private void setUpShapes() {
		FrameDetails fdFullImage= this.fullImagePane.getFrameDetails();
		double dHeadHeight = 50;
		double dHeadWidth = 30;
		double dTailWidth = 40;
		double dTailHeight = 20;
		double dArrowSpaceOut = 15;
		double dTopY = fdFullImage.getHeight();
		double dPosXStarting = 60;
		Color colorFill=Color.BLUE;
		Color colorStroke=Color.AZURE;
		Color colorShadow=Color.BLUEVIOLET;
		
		// left arrow
		MoveTo start = new MoveTo(dPosXStarting, dTopY);
		LineTo lineTo = new LineTo(dPosXStarting - dHeadWidth, dTopY + dArrowSpaceOut + (dTailHeight * 0.5));
		LineTo lineTo2 = new LineTo(dPosXStarting, dTopY + dHeadHeight);
		LineTo lineTo3 = new LineTo(dPosXStarting, dTopY + dArrowSpaceOut + dTailHeight);
		LineTo lineTo4 = new LineTo(dPosXStarting + dTailWidth, dTopY + dArrowSpaceOut + dTailHeight);
		LineTo lineTo5 = new LineTo(dPosXStarting + dTailWidth, dTopY + dArrowSpaceOut);
		LineTo lineTo6 = new LineTo(dPosXStarting, dTopY + dArrowSpaceOut);
		LineTo lastLine = new LineTo(dPosXStarting, dTopY);
		leftArrowPath = new Path(start, lineTo, lineTo2, lineTo3, lineTo4, lineTo5, lineTo6, lastLine);
		leftArrowPath.setStrokeWidth(5.0d);
		leftArrowPath.setStrokeType(StrokeType.OUTSIDE);
		leftArrowPath.setFill(colorFill);
		leftArrowPath.setStrokeLineCap(StrokeLineCap.ROUND);
		leftArrowPath.setStrokeLineJoin(StrokeLineJoin.ROUND);
		leftArrowPath.setTranslateY(dHeadHeight*0.5);
		leftArrowPath.setStroke(colorStroke);

		rightArrowPath = new Path(start, lineTo, lineTo2, lineTo3, lineTo4, lineTo5, lineTo6, lastLine);
		rightArrowPath.setRotate(180.0d);
		rightArrowPath.setStrokeWidth(5.0d);
		rightArrowPath.setStrokeType(StrokeType.OUTSIDE);
		rightArrowPath.setFill(colorFill);
		rightArrowPath.setStrokeLineJoin(StrokeLineJoin.ROUND);
		rightArrowPath.setStrokeLineCap(StrokeLineCap.ROUND);
		rightArrowPath.setTranslateY(dHeadHeight*0.5);
		rightArrowPath.setStroke(colorStroke);

		// set shadow effect
		this.shadowEffect = new DropShadow(25.0d, 0.0d, 0.0d, colorShadow);
		
		// left arrow events
		Map<EventType<? extends Event>, EventHandler<? super Event>> arrowEventsMap = new HashMap<>();
		arrowEventsMap.put(MouseEvent.MOUSE_CLICKED, event -> {
			if (this.fullImagePane != null) {
				this.fullImagePane.previous();
			}
		});

		arrowEventsMap.put(MouseEvent.MOUSE_ENTERED, event -> {
			leftArrowPath.setEffect(shadowEffect);
			leftArrowPath.setFill(colorFill.brighter().brighter());
		});
		arrowEventsMap.put(MouseEvent.MOUSE_EXITED, event -> {
			leftArrowPath.setEffect(null);
			leftArrowPath.setFill(colorFill);
		});

		this.eventHandler.addAllEventHandlers(this.leftArrowPath, arrowEventsMap);
		// right arrow events
		arrowEventsMap = new HashMap<>();

		arrowEventsMap.put(MouseEvent.MOUSE_CLICKED, event -> {
			if (this.fullImagePane != null) {
				this.fullImagePane.next();
			}
		});

		arrowEventsMap.put(MouseEvent.MOUSE_ENTERED, event -> {
			rightArrowPath.setEffect(shadowEffect);
			rightArrowPath.setFill(colorFill.brighter().brighter());
		});
		arrowEventsMap.put(MouseEvent.MOUSE_EXITED, event -> {
			rightArrowPath.setEffect(null);
			rightArrowPath.setFill(colorFill);
		});

		this.eventHandler.addAllEventHandlers(this.rightArrowPath, arrowEventsMap);

		//central circle - pick an image button/icon/shape
		Circle baseCircle = new Circle(0, 0, dHeadHeight);
		Shape midCircle = Shape.subtract(baseCircle, new Circle(0, 0, dHeadHeight * 0.6));
		this.innerCircle = Shape.union(midCircle, new Circle(0, 0, dHeadHeight * 0.35d));
		this.innerCircle.setStrokeWidth(10);
		this.innerCircle.setStrokeType(StrokeType.OUTSIDE);
		this.innerCircle.setFill(colorFill);
		this.innerCircle.setStrokeLineJoin(StrokeLineJoin.ROUND);
		this.innerCircle.setStrokeLineCap(StrokeLineCap.ROUND);
		this.innerCircle.setStroke(colorStroke);
		// setEvents
		arrowEventsMap = new HashMap<>();

		arrowEventsMap.put(MouseEvent.MOUSE_CLICKED, event -> {
			if (this.fullImagePane != null) {
				this.fullImagePane.pickPiture();;
			}
		});

		arrowEventsMap.put(MouseEvent.MOUSE_ENTERED, event -> {
			this.innerCircle.setEffect(shadowEffect);
			this.innerCircle.setFill(colorFill.brighter().brighter());
		});
		arrowEventsMap.put(MouseEvent.MOUSE_EXITED, event -> {
			this.innerCircle.setEffect(null);
			this.innerCircle.setFill(colorFill);
		});

		this.eventHandler.addAllEventHandlers(this.innerCircle, arrowEventsMap);
	}

	public void setContols() {
		initControls();
		if (this.fullImagePane != null) {
			HBox box = new HBox(this.leftArrowPath, this.innerCircle, this.rightArrowPath);
			box.setAlignment(Pos.TOP_CENTER);
			HBox box2=new HBox(this.cbColorMenu);
			box2.setAlignment(Pos.CENTER);
			VBox vbox=new VBox(10.0d,box,box2);
			vbox.setTranslateY(this.fullImagePane.getFrameDetails().getHeight()*0.33d);
			this.fullImagePane.getPane().getChildren().add(vbox);
		}
	}

	public void cleanAll() {
		this.tearDownAll();
	}

	private void tearDownAll() {
		this.eventHandler.clear();
		unsetControls();
	}

	private void unsetControls() {
		this.fullImagePane = null;
		this.segmentsPane = null;
		this.eventHandler = null;
		this.cbColorMenu = null;
		this.btnClear = null;
		this.btnReset = null;
		//tear down shapes
		this.leftArrowPath=null;
		this.rightArrowPath=null;
		this.innerCircle=null;
	}

	// controls getters
	public final ComboBox<RGBEnum> getComboBox() {
		return cbColorMenu;
	}

	public final Shape getPickImageButton() {
		return this.innerCircle;
	}

	public final Button getClearSegmentsButton() {
		return btnClear;
	}

	public final Button getResetButton() {
		return btnReset;
	}

	public final Shape getNextButton() {
		return this.rightArrowPath;
	}

	public final Shape getPreviousButton() {
		return this.leftArrowPath;
	}

}
