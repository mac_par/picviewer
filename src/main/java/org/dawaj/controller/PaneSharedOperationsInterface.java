package org.dawaj.controller;

import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import java.util.logging.Logger;

public interface PaneSharedOperationsInterface {
	void clean(); //content reset
	void clearAll(); //release memory and unset handlers
	Pane getPane(); //return a pane
	void bindStage(Stage stage);
	void setLogger(Logger logger);
}
