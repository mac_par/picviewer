package org.dawaj.controller;

import javafx.geometry.Orientation;
import javafx.geometry.Point2D;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineJoin;
import javafx.scene.shape.StrokeType;
import javafx.stage.Stage;
import javafx.scene.Cursor;
import javafx.scene.image.Image;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import java.io.File;
import java.util.Vector;
import java.util.logging.Logger;
import org.dawaj.controller.eventhandling.PicViewerEventHandler;
import org.dawaj.controller.eventhandling.PicViewerEventInterface;
import org.dawaj.model.image_utils.ImageRect;
import org.dawaj.model.image_utils.ImageUnit;
import org.dawaj.model.image_utils.SizeProperty;
import org.dawaj.model.supplementary.ColorComponent;
import org.dawaj.model.supplementary.DimensionRatio;
import org.dawaj.model.supplementary.FrameDetails;
import org.dawaj.model.supplementary.RGBEnum;
import org.dawaj.view.SegmentedPaneControlInterface;
import org.dawaj.controller.concurrency.ImageTask;
import javafx.scene.layout.Pane;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import java.util.Iterator;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.Future;
import org.dawaj.view.dialog.ImageFileDialogBox;



public class SegmentedImagesPane implements SegmentedPaneControlInterface {
	private final PicViewerEventInterface eventHandler;
	private final Vector<ImageRect> vecDisplayRects;
	private final Vector<ImageUnit> vecImages;
	private final TilePane segmentedPane;
	private final FrameDetails fdFrames;
	private final DimensionRatio drScreen;
	private WritableImage writableImage;
	private Stage stageRoot = null;
	private Logger logPane = null;
	private ObjectProperty<ColorComponent> basicColorComponent = new SimpleObjectProperty<>(new ColorComponent());
	private ThreadPoolExecutor thExec;

	public SegmentedImagesPane() {
		fdFrames = new FrameDetails(20, 100.0d);
		this.drScreen = new DimensionRatio();
		this.drScreen.setScreenSize(1920., 1080.0);
		this.vecDisplayRects = new Vector<>(this.fdFrames.getnFrameAmount());
		this.vecImages = new Vector<>(this.fdFrames.getnFrameAmount());
		segmentedPane = new TilePane(Orientation.HORIZONTAL);
		eventHandler = new PicViewerEventHandler();
		init();
	}

	public SegmentedImagesPane(DimensionRatio dimenSpace, FrameDetails fdFrames) {
		this.fdFrames = fdFrames;
		this.drScreen = dimenSpace;
		this.vecDisplayRects = new Vector<>(this.fdFrames.getnFrameAmount());
		this.vecImages = new Vector<>(this.fdFrames.getnFrameAmount());
		segmentedPane = new TilePane(Orientation.HORIZONTAL);
		eventHandler = new PicViewerEventHandler();
		init();
	}

	private void init() {
		// calculate columns
		int nFramesPerWidth = 0, nFramesPerHeight = 0;
		while (((nFramesPerWidth + 1) * fdFrames.getWidth()) < drScreen.getScreenDimensions().getWidth())
			nFramesPerWidth++;
		// calculate rows
		while (((nFramesPerHeight + 1) * fdFrames.getHeight()) < drScreen.getScreenDimensions().getHeight())
			nFramesPerHeight++;
		// calculate padding
		double dHPadding, dVPadding;
		dHPadding = drScreen.getScreenDimensions().getWidth() / (nFramesPerWidth * fdFrames.getWidth());
		dVPadding = drScreen.getScreenDimensions().getHeight() / (nFramesPerHeight * fdFrames.getHeight());
		segmentedPane.setPrefColumns(nFramesPerWidth);
		segmentedPane.setPrefRows(nFramesPerHeight);
		segmentedPane.setHgap(dHPadding * ++nFramesPerWidth);
		segmentedPane.setVgap(dVPadding * ++nFramesPerHeight);
		segmentedPane.setPrefHeight(drScreen.getScreenDimensions().getHeight());
		segmentedPane.setPrefWidth(drScreen.getScreenDimensions().getWidth());
		segmentedPane.setPadding(new Insets(15.0));
		this.thExec=(ThreadPoolExecutor)Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
		initSetImages();
	}

	private void initSetImages() {
		if ((this.vecDisplayRects != null && this.vecDisplayRects.isEmpty())
				&& (this.vecImages != null && this.vecDisplayRects.isEmpty())) {
			this.basicColorComponent.set(new ColorComponent(RGBEnum.RED));
			for (int i = 0; i < fdFrames.getnFrameAmount(); ++i) {
				ImageUnit iuObject = new ImageUnit(
						new SizeProperty(this.fdFrames.getWidth(), this.fdFrames.getHeight()));
				iuObject.setSupplierFunc(this::getMockImage);
				iuObject.setRGBElement(RGBEnum.RED);
				this.vecImages.add(iuObject);
				ImageRect rect = new ImageRect(iuObject);
				setRectStyle(rect);
				// add to container
				segmentedPane.getChildren().add(rect);
				
				// set events
				eventHandler.addEventHandler(rect, MouseEvent.MOUSE_CLICKED, ev -> {
					if (ev instanceof MouseEvent) {
						//show dialog box
						ImageFileDialogBox.showFileDetails((ImageRect)ev.getSource());
					}
				});
				eventHandler.addEventHandler(rect, MouseEvent.MOUSE_ENTERED, ev -> {
					if (this.stageRoot!=null) {
						this.stageRoot.getScene().setCursor(Cursor.OPEN_HAND);
					}
				});
				eventHandler.addEventHandler(rect, MouseEvent.MOUSE_EXITED, ev -> {
					if (this.stageRoot!=null) {
						this.stageRoot.getScene().setCursor(Cursor.DEFAULT);
					}
				});
				rect.reset();
				this.vecDisplayRects.add(rect);
			}
			eventHandler.setUp();
		}
	}

	private void setRectStyle(ImageRect rect) {
		rect.setStroke(Color.BLACK);
		rect.setStrokeWidth(4);
		rect.setStrokeLineJoin(StrokeLineJoin.ROUND);
		rect.setStrokeType(StrokeType.OUTSIDE);
	}

	@Override
	public synchronized void clean() {
		if (this.vecDisplayRects != null && !this.vecDisplayRects.isEmpty()) {
			this.vecDisplayRects.parallelStream().forEach(rect->rect.reset());
		}
	}

	@Override
	public void clearAll() {
		eventHandler.clear();
		tearDownAll();
	}

	private void tearDownAll() {
		this.thExec.shutdown();
		if (!this.areContainersSetAndNotEmpty())
			return;
		// reset displayed content
		for (ImageRect irTmp : this.vecDisplayRects) {
			irTmp.clear();
		}
		// empty containers
		this.vecDisplayRects.clear();
		this.vecImages.clear();
		this.writableImage = null;
		this.stageRoot = null;
		this.logPane = null;
		this.basicColorComponent = null;
	}

	private void mergeContent() {
		if (this.areContainersSetAndNotEmpty()) {
			Iterator<ImageRect> iterRect = this.vecDisplayRects.iterator();
			Iterator<ImageUnit> iterImg = this.vecImages.iterator();
			while (iterRect.hasNext() && iterImg.hasNext()) {
				iterRect.next().setImageContainer(iterImg.next());
			}
		}
	}

	private synchronized boolean areContainersSetAndNotEmpty() {
		return (this.vecDisplayRects != null && !this.vecDisplayRects.isEmpty())
				&& (this.vecImages != null && !this.vecImages.isEmpty());
	}

	@Override
	public Pane getPane() {
		return segmentedPane;
	}

	@Override
	public void setRGBDescriptor(RGBEnum descriptor) {
		if (this.areContainersSetAndNotEmpty()) {
			this.basicColorComponent.getValue().setColorComponent(descriptor);
			this.vecImages.parallelStream().forEach(obj -> obj.setRGBElement(descriptor));
			this.sortVec();
			this.mergeContent();
		}
	}

	private Image getMockImage() {
		if (writableImage == null) {
			writableImage = new WritableImage((int) fdFrames.getWidth(), (int) fdFrames.getHeight());
			PixelWriter pixWriter = writableImage.getPixelWriter();
			for (int x = 0; x < writableImage.getWidth(); x++) {
				for (int y = 0; y < writableImage.getHeight(); y++) {
					pixWriter.setColor(x, y, Color.TRANSPARENT);
				}
			}
		}
		return writableImage;
	}

	@Override
	public void bindStage(Stage stage) {
		if (stage != null) {
			this.stageRoot = stage;
		}
	}

	@Override
	public synchronized void addImage(File file, Point2D point) {
		if (file != null && this.areContainersSetAndNotEmpty() && point != null) {
			ColorComponent component = this.basicColorComponent.getValue();
			ImageTask newImageTask=this.getImageTask(this.fdFrames, component.getColorComponent(), file, point);
			Future<ImageUnit> future=thExec.submit(newImageTask);
			/*ImageUnit iuObj = new ImageUnit(new SizeProperty(this.fdFrames.getWidth(), this.fdFrames.getHeight()),
					component.getColorComponent(), this::getMockImage, file, point);*/
			
			try {
			
				if (this.update(future.get(300, TimeUnit.MILLISECONDS))) {
					//this.sortVec();
					this.mergeContent();
				}
			}catch(InterruptedException | ExecutionException | TimeoutException ex) {
				logPane.throwing(this.getClass().getName(), "addImage", ex);
			}
		}
	}

	private ImageTask getImageTask(FrameDetails frameDetails, RGBEnum component,File file, Point2D point) {
		ImageTask imageTask=new ImageTask();
		imageTask.setSizeProperty(frameDetails);
		imageTask.setRGB(component);
		imageTask.setSuppFunc(this::getMockImage);
		imageTask.setFile(file);
		imageTask.setPoint(point);
		return imageTask;
	}
	private boolean update(ImageUnit obj) {
		if (!obj.isValid())
			return false;
		
		ImageUnit iuTmp = obj;
		boolean bAdded = false;
		// order elements - bubble sort
		for (int i = 0; i < this.vecImages.size(); i++) {
			// for added object
			if (iuTmp.compareTo(this.vecImages.get(i)) > 0) {
				ImageUnit iuTmp2 = this.vecImages.get(i);
				this.vecImages.set(i, iuTmp);
				iuTmp = iuTmp2;
				if (!bAdded)
					bAdded = true;
			}
		}
		return bAdded;
	}

	private void sortVec() {
		// insert sort
		for (int i = 1; i < this.vecImages.size(); i++) {
			int j = i;
			int k = j - 1;
			while (j >= 0 && k >= 0) {
				if (this.vecImages.get(k).compareTo(this.vecImages.get(j)) < 0) {
					ImageUnit iuTmp = this.vecImages.get(j);
					this.vecImages.set(j, this.vecImages.get(k));
					this.vecImages.set(k, iuTmp);
					j--;
					k--;
				} else {					
					break;
				}
			}
		}
	}

	@Override
	public void setLogger(Logger logger) {
		if (logger != null)
			this.logPane = logger;
	}

}