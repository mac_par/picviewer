package org.dawaj.controller;

import org.dawaj.model.AbstractImageCollector;
import org.dawaj.model.image_utils.ImageUnit;
import org.dawaj.model.supplementary.DimensionRatio;
import org.dawaj.model.supplementary.FrameDetails;
import org.dawaj.model.supplementary.RGBEnum;
import org.dawaj.view.FullPictureControlInterface;
import org.dawaj.view.SegmentedPaneControlInterface;
import java.util.logging.Logger;

import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;
import org.dawaj.controller.navigation.NavigationManager;

public class ImageManager extends AbstractImageCollector{
	private final FullPictureControlInterface fullImagePane;
	private final SegmentedPaneControlInterface segmentsPane;
	private final NavigationManager nmControls;
	
	public ImageManager() {
		segmentsPane = new SegmentedImagesPane();
		fullImagePane = new FullPictureViewPane(segmentsPane);
		this.nmControls=new NavigationManager(fullImagePane, segmentsPane);
		init();
	}

	public ImageManager(DimensionRatio dFstPaneRatio,FrameDetails fdFstFrame,
			DimensionRatio dSndPaneRatio,FrameDetails fdSndFrame) {
		segmentsPane = new SegmentedImagesPane(dSndPaneRatio,fdSndFrame);
		fullImagePane = new FullPictureViewPane(segmentsPane,dFstPaneRatio,fdFstFrame);
		this.nmControls=new NavigationManager(fullImagePane, segmentsPane);
		init();
	}
	
	private void init() {
		fullImagePane.setLogger(getLogger());
		segmentsPane.setLogger(getLogger());
		segmentsPane.setRGBDescriptor(RGBEnum.RED);
	}
	@Override
	public void begin() {
		fullImagePane.begin();
	}

	@Override
	public void end() {
		fullImagePane.end();
	}

	@Override
	public void next() {
		fullImagePane.next();

	}

	@Override
	public void previous() {
		fullImagePane.previous();

	}

	@Override
	public void reset() {
		fullImagePane.clean();
		segmentsPane.clean();
	}

	@Override
	public void clearAll() {
		fullImagePane.clearAll();
		segmentsPane.clearAll();
		if(this.nmControls!=null) {
			this.nmControls.cleanAll();
		}
	}

	@Override
	public void choosePicture() {
		fullImagePane.pickPiture();

	}

	@Override
	public Pane getFullImageFlowPane() {
		if (fullImagePane != null)
			return fullImagePane.getPane();
		else
			return null;
	}

	@Override
	public Pane getSegmentedFlowPane() {
		if (segmentsPane != null)
			return segmentsPane.getPane();
		else
			return null;
	}

	@Override
	public void setRGBDescriptor(RGBEnum descriptor) {
		segmentsPane.setRGBDescriptor(descriptor);
	}

	@Override
	public void bindStage(Stage stage) {
		fullImagePane.bindStage(stage);
		segmentsPane.bindStage(stage);
	}

	@Override
	public void setInitialDirectory(String path) {
		fullImagePane.getFilesAt(path);
	}

	@Override
	public void cleanImage() {
		fullImagePane.begin();
		this.cleanSegments();
	}

	@Override
	public void cleanSegments() {
		segmentsPane.clean();
		
	}

	@Override
	public void setDefaultControls() {
		if(this.nmControls!=null) {
			this.nmControls.setContols();
		}
	}

	@Override
	public ComboBox<RGBEnum> getComboBox() {
		if(this.nmControls!=null) {
			return this.nmControls.getComboBox();
		}else {			
			return null;
		}
	}

	@Override
	public Shape getPickImageButton() {
		if(this.nmControls!=null) {
			return this.nmControls.getPickImageButton();
		}else {			
			return null;
		}
	}

	@Override
	public Button getClearSegmentsButton() {
		if(this.nmControls!=null) {
			return this.nmControls.getClearSegmentsButton();
		}else {			
			return null;
		}
	}

	@Override
	public Button getResetButton() {
		if(this.nmControls!=null) {
			return this.nmControls.getResetButton();
		}else {			
			return null;
		}
	}

	@Override
	public Shape getNextButton() {
		if(this.nmControls!=null) {
			return this.nmControls.getNextButton();
		}else {			
			return null;
		}
	}

	@Override
	public Shape getPreviousButton() {
		if(this.nmControls!=null) {
			return this.nmControls.getPreviousButton();
		}else {			
			return null;
		}
	}

}
