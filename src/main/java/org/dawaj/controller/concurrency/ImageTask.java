package org.dawaj.controller.concurrency;
import java.util.concurrent.Callable;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import org.dawaj.model.image_utils.ImageUnit;
import org.dawaj.model.image_utils.SizeProperty;
import org.dawaj.model.supplementary.FrameDetails;
import org.dawaj.model.supplementary.RGBEnum;
import java.io.File;
import java.util.function.Supplier;


public class ImageTask implements Callable<ImageUnit>{
	private SizeProperty mSizeProp=null;
	private RGBEnum rgbComponent=null;
	private File file=null;
	private Point2D point=null;
	private Supplier<Image> suppFunc;
	
	public final SizeProperty getmSizeProp() {
		return mSizeProp;
	}
	public final void setSizeProperty(SizeProperty mSizeProp) {
		this.mSizeProp = mSizeProp;
	}
	
	public final void setSizeProperty(FrameDetails frameDetails) {
		this.mSizeProp = new SizeProperty(frameDetails.getWidth(),frameDetails.getHeight());
	}
	public final void setRGB(RGBEnum rgbComponent) {
		this.rgbComponent = rgbComponent;
	}
	
	public final void setFile(File file) {
		this.file = file;
	}
	
	public final void setPoint(Point2D point) {
		this.point = point;
	}
	
	
	public final void setSuppFunc(Supplier<Image> supplier) {
		this.suppFunc = supplier;
	}
	
	
	@Override
	public ImageUnit call() throws Exception {
		ImageUnit iuImage=null;	
		if(mSizeProp!=null && rgbComponent!=null && file !=null && point !=null && suppFunc !=null) {
				iuImage=new ImageUnit(mSizeProp,rgbComponent,suppFunc,file,point);
			}
		return iuImage;
	}
}
