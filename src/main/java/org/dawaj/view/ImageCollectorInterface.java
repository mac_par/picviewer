package org.dawaj.view;
import org.dawaj.model.supplementary.RGBEnum;

import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;

public interface ImageCollectorInterface {
	void begin();
	void end();
	void next();
	void previous();
	void choosePicture();
	void reset();//reset content
	void clearAll(); //release memory and unset event handlers
	void setInitialDirectory(String path);
	Pane getFullImageFlowPane();
	Pane getSegmentedFlowPane();
	void setRGBDescriptor(RGBEnum descriptor);
	void bindStage(Stage stage);
	void cleanImage();
	void cleanSegments();
	//navigation manager methods
	void setDefaultControls();
	ComboBox<RGBEnum> getComboBox();
	Shape getPickImageButton();
	Button getClearSegmentsButton();
	Button getResetButton();
	Shape getNextButton();
	Shape getPreviousButton();
}
