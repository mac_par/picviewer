package org.dawaj.view;
import org.dawaj.controller.PaneSharedOperationsInterface;
import org.dawaj.model.supplementary.FrameDetails;

public interface FullPictureControlInterface extends PaneSharedOperationsInterface{
	String EXTENSIONS=".jpeg:.jpg:.gif:.png";
	double IMAGE_WIDTH_DEF = 256;
	double IMAGE_HEIGHT_DEF = 512;
	double VERTICAL_PADDING = 15d;
	double HORIZONTAL_PADDING = 15d;
	String MAIN_DIR = "PicViewer";
	String MAIN_FILE = "settings.properties";
	String PROPS_HEAD_KEY="images.count";
	String PROPS_IMG_KEY="image";
	String PROPS_IMG_KEY_FORMAT=".img%03d";
	String PROPS_IMG_LAST=".lastImg";
	String PROPS_MAIN_DIR="img.main_dir";
	void begin();
	void end();
	void next();
	void previous();
	void pickPiture();
	void getFilesAt(String path);
	FrameDetails getFrameDetails();
}
