package org.dawaj.view.dialog;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import org.dawaj.model.image_utils.ImageRect;
import javafx.geometry.Dimension2D;
import java.util.StringJoiner;
import java.util.logging.Logger;
import javafx.stage.WindowEvent;
import java.io.File;

public class ImageFileDialogBox {
	private static final String TITLE_FORMAT = "File '%s' details window";
	private static final Dimension2D SCREEN_DIMENSIONS = new Dimension2D(500.0d, 200.0d);
	private static final String[] DESC_FORMATS = { "File: %s", "Location: %s", "Resolution: %s pixels",
			"Color component: %s", "Density: %s" };
	private static final int ROWS = DESC_FORMATS.length, COLUMNS = 15;
	private static final String INFO_IMG = "info.png", IMG_DIR = "pictures";
	private static TextArea textBox = null;
	private static ImageView imageView = null;

	public static void showFileDetails(final ImageRect imageRect) {
		if (imageRect == null || imageRect.isEmpty())
			return;
		GridPane dialogRoot = new GridPane();
		Stage stage = new Stage();
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setScene(new Scene(dialogRoot, SCREEN_DIMENSIONS.getWidth(), SCREEN_DIMENSIONS.getHeight()));
		// set grid pane
		setGrid(dialogRoot, imageRect);

		stage.setTitle(String.format(TITLE_FORMAT, imageRect.getFilename()));
		stage.show();
		stage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, event -> tearDown());
	}

	private static void tearDown() {
		textBox = null;
		imageView = null;
	}

	private static void setGrid(GridPane grid, final ImageRect imageRect) {
		grid.setHgap(10);
		grid.setVgap(10);
		textBox = new TextArea();
		textBox.setPrefColumnCount(COLUMNS);
		textBox.setPrefRowCount(ROWS);
		setText(imageRect, textBox);

		imageView = new ImageView(String.format("%1$c%2$s%1$c%3$s", File.separatorChar, IMG_DIR, INFO_IMG));
		// lay it out in the grid
		// grid.setGridLinesVisible(true);
		Label infoLb = new Label("File info");
		infoLb.setStyle("-fx-font-size:17pt; -fx-font-weight:bold; -fx-font-family:Verdana;");
		grid.add(infoLb, 2, 1);
		grid.add(imageView, 2, 3,2,2);
		ScrollPane scrollPane = new ScrollPane(textBox);
		scrollPane.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
		scrollPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		grid.add(scrollPane, 6,2,COLUMNS,ROWS);
	}

	private static void setText(final ImageRect iRect, TextArea outTextArea) {
		StringJoiner joiner = new StringJoiner("\n");
		if (iRect.isEmpty()) {
			joiner.add("File is no available");
		} else {
			try {
				String[] properties = iRect.getImageDescription().split(";");
				for (int i = 1; i < properties.length; i++) {
					joiner.add(String.format(DESC_FORMATS[i - 1], properties[i]));
				}
			} catch (ArrayIndexOutOfBoundsException ex) {
				Logger.getGlobal().throwing(ImageFileDialogBox.class.getName(), "getDescriptionString", ex);
				joiner.add("Error has occured. Please inform developer about this bug!!");
			} finally {
				outTextArea.setText(joiner.toString());
				outTextArea.setEditable(false);
				outTextArea.setWrapText(false);
				outTextArea.setFont(Font.font("Verdana", 16));
			}
		}
	}

}
