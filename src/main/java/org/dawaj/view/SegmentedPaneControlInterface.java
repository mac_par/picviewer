package org.dawaj.view;
import org.dawaj.model.supplementary.RGBEnum;

import javafx.geometry.Point2D;
import java.io.File;
import org.dawaj.controller.PaneSharedOperationsInterface;

public interface SegmentedPaneControlInterface extends PaneSharedOperationsInterface{
	void addImage(File file,Point2D point);
	void setRGBDescriptor(RGBEnum descriptor);
}
