package org.dawaj.model.supplementary;

import javafx.geometry.Dimension2D;

public class DimensionRatio {
	private static final double MAX_RATIO = 0.7d;
	private final double dHeightRatio;
	private final double dWidthRatio;
	private Dimension2D screenDimen;

	public DimensionRatio() {
		this.dHeightRatio = MAX_RATIO;
		this.dWidthRatio = MAX_RATIO;
	}

	public DimensionRatio(double dWidthRatio, double dHeightRatio) {
		this.dWidthRatio = validateRatio(dWidthRatio);
		this.dHeightRatio = validateRatio(dHeightRatio);
	}

	private static double validateRatio(double value) {
		return (Double.compare(value, MAX_RATIO) == 1) ? MAX_RATIO : value;
	}

	public void setScreenSize(double scrWidth, double srcHeight) {
		this.screenDimen = new Dimension2D(scrWidth * this.dWidthRatio, srcHeight * this.dHeightRatio);
	}

	public void setScreenSize(Dimension2D srcDimen) {
		this.screenDimen = new Dimension2D(srcDimen.getWidth() * this.dWidthRatio,
				srcDimen.getHeight() * this.dHeightRatio);
	}

	public Dimension2D getScreenDimensions() {
		return this.screenDimen;
	}

	public double getdHeightRatio() {
		return dHeightRatio;
	}

	public double getdWidthRatio() {
		return dWidthRatio;
	}
	

}
