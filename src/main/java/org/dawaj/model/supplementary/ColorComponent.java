package org.dawaj.model.supplementary;

public class ColorComponent {
	private RGBEnum colorComponent=RGBEnum.NA;
	
	public ColorComponent() {}
	public ColorComponent(RGBEnum colorComponent) {
		this.setColorComponent(colorComponent);
	}
	public final RGBEnum getColorComponent() {
		return colorComponent;
	}
	public final void setColorComponent(RGBEnum colorComponent) {
		this.colorComponent = (colorComponent==null)?RGBEnum.NA:colorComponent;
	}
	
	public boolean isValid() {
		return this.colorComponent!=RGBEnum.NA;
	}
	public void reset() {
		this.setColorComponent(RGBEnum.NA);
	}
	
	@Override
	public String toString() {
		return this.colorComponent.toString();
	}
}
