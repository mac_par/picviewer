package org.dawaj.model.supplementary;
import javafx.geometry.Dimension2D;

public class FrameDetails {
	private final int nFrameAmount;
	private final Dimension2D dimenFrameSize;
	
	public FrameDetails() {
		this.nFrameAmount=1;
		this.dimenFrameSize=new Dimension2D(1.0d,1.0d);
	}
	
	public FrameDetails(int frameAmount,double frameSize) {
		this.nFrameAmount=frameAmount;
		this.dimenFrameSize=new Dimension2D(frameSize,frameSize);
	}
	
	public FrameDetails(int frameAmount,double dWidth,double dHeight) {
		this.nFrameAmount=frameAmount;
		this.dimenFrameSize=new Dimension2D(dWidth, dHeight);
	}
	
	public FrameDetails(int frameAmount,Dimension2D dimenSize) {
		this.nFrameAmount=frameAmount;
		this.dimenFrameSize=dimenSize;
	}

	public double getHeight() {
		return this.dimenFrameSize.getHeight();
	}
	
	public double getWidth() {
		return this.dimenFrameSize.getWidth();
	}
	
	public int getnFrameAmount() {
		return nFrameAmount;
	}

	public Dimension2D getdFrameSize() {
		return dimenFrameSize;
	}
	
}
