package org.dawaj.model.supplementary;

public enum RGBEnum {
	RED("Red"),GREEN("Green"),BLUE("Blue"),NA("Not Setted");
	private String strComponentName;
	
	private RGBEnum(String name) {
		this.strComponentName=name;
	}
	
	@Override
	public String toString() {
		return this.strComponentName;
	}
}
