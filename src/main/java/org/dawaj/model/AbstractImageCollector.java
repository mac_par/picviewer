package org.dawaj.model;

import java.util.logging.Logger;

import org.dawaj.view.ImageCollectorInterface;

public abstract class AbstractImageCollector implements ImageCollectorInterface {
	private final Logger logManager;

	public AbstractImageCollector() {
		this.logManager = Logger.getLogger(this.getClass().getName());
	}
	
	protected Logger getLogger() {
		return this.logManager;
	}
}
