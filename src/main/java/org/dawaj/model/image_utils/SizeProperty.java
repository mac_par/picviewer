package org.dawaj.model.image_utils;

public class SizeProperty {
	private Double propDoubleXSize;
	private Double propDoubleYSize;

	public SizeProperty(double valX, double valY) {
		propDoubleXSize = new Double((Double.compare(valX, 0.0) <= 0) ? 1.0d : valX);
		propDoubleYSize = new Double((Double.compare(valY, 0.0) <= 0) ? 1.0d : valY);
	}

	// getters for properties
	public final int getXIntSizeProperty() {
		return propDoubleXSize.intValue();
	}

	public final int getIntYSizeProperty() {
		return propDoubleYSize.intValue();
	}

	public final Double getXSize() {
		return propDoubleXSize;
	}

	public final Double getYSize() {
		return propDoubleYSize;
	}

	public final void setPropDoubleXSize(Double propDoubleXSize) {
		this.propDoubleXSize = propDoubleXSize;
	}

	public final void setPropDoubleYSize(Double propDoubleYSize) {
		this.propDoubleYSize = propDoubleYSize;
	}

	@Override
	public String toString() {
		return String.format("%.2f x %.2f", propDoubleXSize, propDoubleYSize);
	}

	@Override
	public int hashCode() {
		return this.propDoubleXSize.hashCode() + propDoubleYSize.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!obj.getClass().equals(SizeProperty.class))
			return false;
		SizeProperty szProp=(SizeProperty)obj;
		return this.propDoubleXSize.equals(szProp.propDoubleXSize)&& this.propDoubleYSize.equals(szProp.propDoubleYSize);
	}

}
