package org.dawaj.model.image_utils;

import org.dawaj.model.image_utils.AbstractImageContainerBase;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;
import java.util.logging.Logger;
import org.dawaj.model.image_utils.SizeProperty;
import org.dawaj.model.supplementary.RGBEnum;
import java.util.StringJoiner;
import java.util.function.Supplier;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import java.util.Objects;
import java.io.File;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.ObjectProperty;
import javafx.geometry.Point2D;
import org.dawaj.model.supplementary.ColorComponent;
import java.io.FileInputStream;

public class ImageUnit extends AbstractImageContainerBase implements Comparable<ImageUnit> {
	private static final double MIN_FRAME_SIZE = 41.0d;
	private static Image defaultImage;
	private static final String DIR = "pictures", IMG_FILENAME = "image_not_available.png";
	private ColorComponent basicColorComponent = new ColorComponent();
	private ObjectProperty<ImagePattern> imagePattern = null;

	public ImageUnit() {
		super(new SizeProperty(MIN_FRAME_SIZE, MIN_FRAME_SIZE));
		refresh();
	}

	public ImageUnit(SizeProperty sizeProperty) {
		super(sizeProperty);
		refresh();
	}

	public ImageUnit(SizeProperty sizeProperty, RGBEnum colorElement) {
		this(sizeProperty);
		setRGBElement(colorElement);
		refresh();
	}

	public ImageUnit(SizeProperty sizeProperty, RGBEnum colorElement, Supplier<Image> supplierMockImage, File file,
			Point2D point) {
		super(sizeProperty, supplierMockImage, file, point);
		setRGBElement(colorElement);
		refresh();
	}

	@Override
	public int compareTo(ImageUnit obj) {
		return Double.compare(this.getPixelDensity(), obj.getPixelDensity());
	}

	private synchronized double getPixelDensity() {
		double totalDensity = 0;
		Image tmpImage = getImage();
		if (tmpImage != null) {
			int width = super.sizeProperty().getXSize().intValue();
			int height = super.sizeProperty().getYSize().intValue();
			PixelReader pixelReader = tmpImage.getPixelReader();
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					totalDensity += getStandardColorDensity(pixelReader.getColor(x, y));
				}
			}
		}
		return totalDensity;
	}

	private synchronized double getStandardColorDensity(Color color) {
		double density;
		switch (basicColorComponent.getColorComponent()) {
		case RED:
			density = color.getRed();
			break;
		case GREEN:
			density = color.getGreen();
			break;
		case BLUE:
			density = color.getBlue();
			break;
		case NA:
		default:
			density = 0;
		}
		return (!this.isValid()) ? 0 : density;
	}

	// =======================================================================
	// Color Component section
	public synchronized void setRGBElement(RGBEnum colorEntity) {
		if (this.basicColorComponent == null) {
			this.basicColorComponent = new ColorComponent();
		}
		this.basicColorComponent.setColorComponent((colorEntity != null) ? colorEntity : RGBEnum.NA);
	}

	// ======================================================================

	@Override
	public synchronized void clear() {
		if (ImageUnit.defaultImage != null)
			ImageUnit.defaultImage = null;
		this.basicColorComponent.reset();
		this.imagePattern.setValue(null);
		super.clear();
	}

	@Override
	public void setImage(Image image) {
		super.setImage(image, new Point2D(0.0, 0.0));
		refresh();
	}

	@Override
	public void setImage(Image image, Point2D point) {
		super.setImage(image, point);
		refresh();
	}

	@Override
	public File getFile() {
		return super.getFile();
	}

	@Override
	public String getFileName() {
		return super.getFileName();
	}

	@Override
	public String getPath() {
		return super.getPath();
	}

	@Override
	protected Image getDefaultImage() {
		if (ImageUnit.defaultImage == null) {
			try{
				String path = String.format("%1$c%2$s%1$c%3$s", File.separatorChar, DIR, IMG_FILENAME);
				double dWidth = this.sizeProperty().getXSize();
				double dHeight = this.sizeProperty().getYSize();
				ImageUnit.defaultImage = new Image(path, dWidth, dHeight,
						true, true);
			} catch (Exception ex) {
				Logger.getGlobal().throwing(this.getClass().getName(), "getDefaultImage", ex);
			}
		}
		return ImageUnit.defaultImage;
	}

	@Override
	public ObjectProperty<? extends Paint> imagePaintProperty() {
		return this.imagePattern;
	}

	@Override
	public ObjectProperty<? extends Image> imageProperty() {
		return null;
	}

	@Override
	protected void refresh() {
		if (this.imagePattern == null) {
			this.imagePattern = new SimpleObjectProperty<ImagePattern>(this, "image pattern",
					new ImagePattern(this.getImage()));
		} else {
			this.imagePattern.set(new ImagePattern(this.getImage()));
		}
	}

	@Override
	public synchronized boolean setByFile(File file) {
		if (super.setByFile(file)) {
			refresh();
			return true;
		}
		return false;
	}

	@Override
	public synchronized boolean setByFileAndPoint(File file, Point2D point) {
		if (super.setByFileAndPoint(file, point)) {
			refresh();
			return true;
		}
		return false;
	}

	// toString: super.toString();color component;color density
	@Override
	public String toString() {
		StringJoiner joiner = new StringJoiner(";");
		joiner.add(super.toString());
		if (this.basicColorComponent != null) {
			if (this.basicColorComponent.isValid())
				joiner.add(String.format("%s;%.2f", this.basicColorComponent.toString(), this.getPixelDensity()));
		}
		return joiner.toString();
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.basicColorComponent, this.imagePattern) + super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj))
			return false;
		if (!obj.getClass().equals(ImageUnit.class))
			return false;
		ImageUnit iuObj = (ImageUnit) obj;
		return Objects.equals(this.basicColorComponent, iuObj.basicColorComponent)
				&& Objects.equals(this.imagePattern, iuObj.imagePattern);
	}

}
