package org.dawaj.model.image_utils;

import java.io.File;
import java.util.Objects;
import java.util.logging.Logger;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;


public class FullImageUnit extends AbstractImageContainerBase {
	private static final String DIR = "pictures", IMG_FILENAME = "image_not_available.png";
	private static Image defaultImage;
	private ObjectProperty<ImagePattern> imagePattern=new SimpleObjectProperty<>(this,"image pattern",null);

	public FullImageUnit(SizeProperty sizeProperty) {
		super(sizeProperty);
		refresh();
	}

	
	@Override
	public void clear() {
		if(FullImageUnit.defaultImage!= null) {
			FullImageUnit.defaultImage=null;
		}
		this.imagePattern.set(null);
		super.clear();
	}
	
	@Override
	protected Image getDefaultImage() {
		if (FullImageUnit.defaultImage == null) {
			try {
				String path = String.format("%1$c%2$s%1$c%3$s", File.separatorChar, DIR, IMG_FILENAME);
				double dWidth = this.sizeProperty().getXSize();
				double dHeight = this.sizeProperty().getYSize();
				FullImageUnit.defaultImage = new Image(FullImageUnit.class.getResource(path).toExternalForm(), dWidth,
						dHeight, true, true);
			} catch (Exception ex) {
				Logger.getGlobal().throwing(this.getClass().getName(), "getDefaultImage", ex);
			}
		}
		return FullImageUnit.defaultImage;
	}
	@Override
	public ObjectProperty<? extends Paint> imagePaintProperty() {
		return this.imagePattern;
	}

	@Override
	public ObjectProperty<? extends Image> imageProperty() {
		return null;
	}

	@Override
	protected void refresh() {
		if(this.imagePattern==null) {
			this.imagePattern=new SimpleObjectProperty<>(this,"image pattern",new ImagePattern(this.getImage()));
		}else {
			this.imagePattern.set(new ImagePattern(this.getImage()));
		}
	}

	@Override
	public boolean setByFile(File file) {
		if(super.setByFile(file)) {
			refresh();
			return true;
		}
		return false;
	}

	@Override
	public boolean setByFileAndPoint(File file, Point2D point) {
		if(super.setByFileAndPoint(file, point)) {
			refresh();
			return true;
		}
		return false;
	}
	@Override
	public void setImage(Image image) {
		super.setImage(image, new Point2D(0.0, 0.0));
		refresh();
	}


	@Override
	public void setImage(Image image, Point2D point) {
		super.setImage(image, point);
		refresh();
	}


	@Override
	public int hashCode() {
		
		return Objects.hashCode(this.imagePattern)+super.hashCode();
	}


	@Override
	public boolean equals(Object obj) {
		if(!super.equals(obj))
			return false;
		if(!obj.getClass().equals(FullImageUnit.class))
			return false;
		FullImageUnit fiuObj=(FullImageUnit)obj;
		return Objects.equals(this.imagePattern,fiuObj.imagePattern);
	}
	
}
