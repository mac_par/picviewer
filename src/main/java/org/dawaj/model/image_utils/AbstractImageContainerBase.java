package org.dawaj.model.image_utils;

import javafx.scene.image.Image;

import javafx.scene.paint.Paint;
import java.io.File;
import java.io.IOException;
import java.io.FileInputStream;
import org.dawaj.model.image_utils.SizeProperty;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.beans.property.ObjectProperty;
import javafx.geometry.Point2D;
import static java.lang.Math.*;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.function.Supplier;

public abstract class AbstractImageContainerBase {
	private Image image;
	private final SizeProperty sizeImageProperty;
	private volatile File filename;
	private volatile boolean bValid = false;
	private Supplier<Image> supplierMockImage = null;

	public AbstractImageContainerBase(SizeProperty sizeProp) {
		this.sizeImageProperty = sizeProp;
		reset();
	}

	public AbstractImageContainerBase(Image image, SizeProperty sizeProp) {
		this.sizeImageProperty = sizeProp;
		this.setImage(image, new Point2D(0.0, 0.0));
		reset();
	}

	public AbstractImageContainerBase(SizeProperty sizeProp, Supplier<Image> supplierMockImage, File file,
			Point2D point) {
		this.sizeImageProperty = sizeProp;
		this.supplierMockImage = supplierMockImage;
		this.setByFileAndPoint(file, point);
	}

	// cleaning up the instance
	public void clear() {
		this.bValid = false;
		this.filename = null;
		this.image = null;
		this.supplierMockImage = null;
	}

	public Image getImage() {
		return this.image;
	}

	public void setImage(Image image) {
		this.setImage(image, new Point2D(0.0, 0.0));
	}

	public void setImage(Image image, Point2D point) {
		if (image != null && point != null) {
			applyImage(image, point);
		}
	}

	private void applyImage(Image srcImage, Point2D point) {
		WritableImage writableImage = new WritableImage(this.sizeImageProperty.getXSize().intValue(),
				this.sizeImageProperty.getYSize().intValue());
		PixelReader pixelReader = srcImage.getPixelReader();
		PixelWriter pixelWriter = writableImage.getPixelWriter();
		this.adjustPointPerImage(srcImage, point);

		// copy pixels
		try {
			double dXMax = min(this.sizeImageProperty.getXSize(), srcImage.getWidth());
			double dYMax = min(this.sizeImageProperty.getYSize(), srcImage.getHeight());
			int nPosX = (int) point.getX();
			int nPosY = (int) point.getY();

			for (int x = 0; x < dXMax; x++) {
				for (int y = 0; y < dYMax; y++) {
					Color cool = pixelReader.getColor(nPosX + x, nPosY + y);
					pixelWriter.setColor(x, y, cool);
				}
			}
			this.image = writableImage;
		} catch (IndexOutOfBoundsException ex) {
			reset();
		} catch (Exception ex) {
			reset();
		}
	}

	private void adjustPointPerImage(Image passedImage, Point2D point) {
		double offsetX = passedImage.getWidth() - (point.getX() + this.sizeImageProperty.getXSize());
		double offsetY = passedImage.getHeight() - (point.getY() + this.sizeImageProperty.getYSize());
		if (Double.compare(offsetX, 0.0) == -1) {
			point.add(offsetX, 0);
		}

		if (Double.compare(offsetY, 0.0) == -1) {
			point.add(0, offsetY);
		}
	}

	public final SizeProperty sizeProperty() {
		return this.sizeImageProperty;
	}

	public File getFile() {
		return filename;
	}

	public String getFileName() {
		if (filename == null || !filename.exists())
			return "";
		else
			return filename.getName();
	}

	public String getPath() {
		if (filename == null)
			return "";
		else {
			return filename.getParent();
		}
	}

	// initialize an image with only a file
	public boolean setByFile(File file) {
		return this.setByFileAndPoint(file, new Point2D(0.0, 0.0));
	}

	// initialize an image on basis of both file and a initial dimension point
	public synchronized boolean setByFileAndPoint(File file, Point2D point) {
		if (file == null || !file.exists()) {
			reset();
			return false;
		} else {
			try (FileInputStream fis = new FileInputStream(file)) {
				this.filename = file;
				Image srcImage = new Image(fis);
				this.bValid = true;
				setImage(srcImage, point);
			} catch (IOException ex) {
				reset();
				return false;
			}
			return true;
		}
	}

	// reset container
	public void reset() {
		this.bValid = false;
		this.filename = null;
		this.resetImage();
	}

	protected void resetImage() {
		this.setImage(this.getMockImage(), new Point2D(0.0, 0.0));
	}

	public boolean isValid() {
		return this.bValid;
	}

	protected Image getMockImage() {
		return (this.supplierMockImage == null) ? this.getDefaultImage() : this.supplierMockImage.get();
	}

	public void setSupplierFunc(Supplier<Image> supplier) {
		if (supplier != null)
			this.supplierMockImage = supplier;
	}

	abstract protected Image getDefaultImage();

	abstract public ObjectProperty<? extends Paint> imagePaintProperty();

	abstract public ObjectProperty<? extends Image> imageProperty();

	abstract protected void refresh();

	// ====================================================================
	// abstract public String toString();
	// format validation state(boolean);filename:file path to the file;resolution -
	// last three
	// elements appear if picture is valid
	@Override
	public String toString() {
		StringJoiner joiner = new StringJoiner(";");
		joiner.add(Boolean.toString(this.bValid));
		if (this.bValid)
			joiner.add(String.format("%s;%s;%s", this.filename.getName(), this.filename.getParent(),
					this.sizeImageProperty));
		return joiner.toString();
	}

	// ==================================================
	@Override
	public int hashCode() {
		return Objects.hash(this.image, this.bValid, this.filename, this.sizeImageProperty, this.supplierMockImage);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof AbstractImageContainerBase))
			return false;
		AbstractImageContainerBase abst = (AbstractImageContainerBase) obj;
		return Objects.equals(this.image, abst.image) && Objects.equals(this.bValid, abst.bValid)
				&& Objects.equals(this.filename, abst.filename)
				&& Objects.equals(this.sizeImageProperty, abst.sizeImageProperty)
				&& Objects.equals(this.supplierMockImage, abst.supplierMockImage);
	}
}