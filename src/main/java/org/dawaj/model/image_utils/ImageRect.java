package org.dawaj.model.image_utils;

import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.beans.property.ObjectProperty;
import java.io.File;
import java.lang.Comparable;
import java.util.function.Supplier;

import org.dawaj.model.image_utils.AbstractImageContainerBase;
import org.dawaj.model.supplementary.RGBEnum;
import org.dawaj.model.supplementary.ColorComponent;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.input.MouseEvent;

public class ImageRect extends Rectangle {
	private AbstractImageContainerBase imageUnit;
	private Tooltip tooltip = null;

	public ImageRect(AbstractImageContainerBase containerInstance) {
		super(containerInstance.sizeProperty().getXSize(), containerInstance.sizeProperty().getXSize());
		imageUnit = containerInstance;
		setUpBindings();
	}

	private void setUpBindings() {
		if (this.fillProperty().isBound()) {
			this.fillProperty().unbind();
		}
		this.fillProperty().bind(this.imageUnit.imagePaintProperty());
		if (this.tooltip == null) {
			this.tooltip = new Tooltip();
			Tooltip.install(this, tooltip);
		}
		tooltip.setText(this.toString());
	}

	private void tearDownBindings() {
		if (this.fillProperty().isBound())
			this.fillProperty().unbind();
		if (this.tooltip != null) {
			tooltip.setText(null);
			Tooltip.uninstall(this, tooltip);
			tooltip = null;
		}
	}

	public Tooltip getTooltip() {

		return tooltip;
	}

	public void turnOffTooltip() {
		Tooltip.uninstall(this, this.tooltip);
		this.tooltip = null;
	}

	public void turnOnTooltip() {
		if (this.tooltip == null) {
			this.tooltip = new Tooltip();
			Tooltip.install(this, this.tooltip);
		}
	}

	public void setRGBDescriptor(RGBEnum rgbDescript) {
		if (imageUnit.getClass().equals(ImageUnit.class)) {
			((ImageUnit) imageUnit).setRGBElement(rgbDescript);
		}
	}

	public void clear() {
		this.tearDownBindings();
		imageUnit.reset();
		this.imageUnit = null;
	}

	public void refresh() {
		this.imageUnit.refresh();
	}

	public void refreshTooltip() {
		if (this.tooltip == null) {
			tooltip.setText(this.imageUnit.toString());
		}
	}

	public void reset() {
		this.imageUnit.reset();
	}

	public File getCurrentFile() {
		return imageUnit.getFile();
	}

	public void setFile(File file) {
		this.imageUnit.setByFile(file);
	}

	public boolean setFileFromPoint(File file, Point2D point) {
		return (point == null) ? this.imageUnit.setByFile(file) : this.imageUnit.setByFileAndPoint(file, point);
	}

	public final SizeProperty getSizeProperty() {
		return this.imageUnit.sizeProperty();
	}

	public void setImageContainer(AbstractImageContainerBase container) {
		if (container != null) {
			this.imageUnit = container;
			setUpBindings();
		}
	}

	public Image getImage() {
		return this.imageUnit.getImage();
	}

	public String getSize() {
		return this.imageUnit.sizeProperty().toString();
	}

	public String getFilename() {
		return this.imageUnit.getFileName();
	}

	public String getPath() {
		return this.imageUnit.getPath();
	}

	@Override
	public String toString() {
		String strFile = this.getFilename();
		if (strFile.length() != 0)
			return String.format("File: %s", strFile);
		else
			return "No image";
	}

	public String getImageDescription() {
		return this.imageUnit.toString();
	}

	public Dimension2D getImageSize() {
		return new Dimension2D(imageUnit.sizeProperty().getXSize(), imageUnit.sizeProperty().getYSize());
	}

	public boolean isEmpty() {
		return !this.imageUnit.isValid();
	}

	public void setMockImageHandler(Supplier<Image> func) {
		this.imageUnit.setSupplierFunc(func);
	}
}
